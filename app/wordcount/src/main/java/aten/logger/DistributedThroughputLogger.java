package aten.logger;

import aten.App.SocketTextStreamWordCount;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by souza on 04/01/18.
 */
public class DistributedThroughputLogger implements FlatMapFunction<String, SocketTextStreamWordCount.WordWithCount> {
    private static final Logger LOG = LoggerFactory.getLogger(ThroughputLogger.class);

    private long totalReceived = 0;
    private long initialSecond = System.nanoTime();
    private long lastTotalReceived = 0;
    private long lastLogTimeMs = -1;
    private long logfreq;

    public DistributedThroughputLogger(long logfreq) {
        this.logfreq = logfreq;
    }

    @Override
    public void flatMap(String element, Collector<SocketTextStreamWordCount.WordWithCount> collector) throws Exception {
        int elements = element.split("\\r?\\n").length;
        totalReceived+=elements;
        final long oneSecond = 1000000000;
        if (System.nanoTime() - initialSecond >= oneSecond){
            // throughput over entire time
            long now = System.currentTimeMillis();

            if(lastLogTimeMs == -1) {
                // init (the first)
                lastLogTimeMs = now;
                lastTotalReceived = totalReceived;
            } else {
                long timeDiff = now - lastLogTimeMs;
                long elementDiff = totalReceived - lastTotalReceived;
                double ex = (1000/(double)timeDiff);
                LOG.info("During the last {} ms, we received {} elements. That's {} elements/second/core. {} MB/sec/core. GB received {}",
                        timeDiff, elementDiff, elementDiff*ex, elementDiff*ex*element.length() / 1024 / 1024, (totalReceived * element.length()) / 1024 / 1024 / 1024);
                // reinit
                lastLogTimeMs = now;
                lastTotalReceived = totalReceived;
            }
            initialSecond = System.nanoTime();
        }
    }
}
