package aten.logger;

/**
 * Created by souza on 14/08/17.
 */
import aten.App.SocketTextStreamWordCount;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThroughputLogger implements FlatMapFunction<SocketTextStreamWordCount.WordWithCount, SocketTextStreamWordCount.WordWithCount> {

    private static final Logger LOG = LoggerFactory.getLogger(ThroughputLogger.class);

    private long totalReceived = 0;
    private long initialSecond = System.nanoTime();
    private long lastTotalReceived = 0;
    private long lastLogTimeMs = -1;
    private long logfreq;
    private long begin = 0;

    public ThroughputLogger(long logfreq) {
        this.logfreq = logfreq;
    }

    @Override
    public void flatMap(SocketTextStreamWordCount.WordWithCount element, Collector<SocketTextStreamWordCount.WordWithCount> collector) throws Exception {
        //int elements = 1;
        if(totalReceived == 0){
            begin = System.currentTimeMillis();
        }
        totalReceived+=1;
        final long oneSecond = 1000000000;
        if (System.nanoTime() - initialSecond >= oneSecond){
            /*if(System.currentTimeMillis() - begin > 3600000){
                LOG.info("Begin {} and final {}", begin, System.currentTimeMillis());
                throw new InterruptedException();
            }*/
            // throughput over entire time
            long now = System.nanoTime();

            if(lastLogTimeMs == -1) {
                // init (the first)
                lastLogTimeMs = now;
                lastTotalReceived = totalReceived;
            } else {
                long timeDiff = now - lastLogTimeMs;
                long elementDiff = totalReceived - lastTotalReceived;
                double ex = (1000/(double)timeDiff);
                //LOG.info("During the last {} ms, we received {} elements. That's {} elements/second/core. {} MB/sec/core. GB received {}",
                //        timeDiff, elementDiff, elementDiff*ex, elementDiff*ex*1 / 1024 / 1024, (totalReceived * 1) / 1024 / 1024 / 1024);
                //LOG.info("Total: {}", totalReceived);
                // reinit
                lastLogTimeMs = now;
                lastTotalReceived = totalReceived;

                if (totalReceived >= 632967806){
                    LOG.info("Total: {}", System.nanoTime() - begin);
                    throw new InterruptedException();
                }

            }
            initialSecond = System.nanoTime();
        }
    }
}