package aten.App;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import aten.logger.DistributedThroughputLogger;
import aten.logger.ThroughputLogger;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.Partitioner;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.runtime.plugable.SerializationDelegate;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import org.apache.flink.streaming.runtime.partitioner.StreamPartitioner;
import org.apache.flink.streaming.runtime.streamrecord.StreamRecord;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.apache.flink.util.Collector;

/**
 * This example shows an implementation of WordCount with data from a text
 * socket. To run the example make sure that the service providing the text data
 * is already up and running.
 * <p>
 * <p>
 * To start an example socket text stream on your local machine run netcat from
 * a command line: <code>nc -lk 9999</code>, where the parameter specifies the
 * port number.
 * <p>
 * <p>
 * <p>
 * Usage:
 * <code>SocketTextStreamWordCount &lt;hostname&gt; &lt;port&gt;</code>
 * <br>
 * <p>
 * <p>
 * This example shows how to:
 * <ul>
 * <li>use StreamExecutionEnvironment.socketTextStream
 * <li>write a simple Flink program
 * <li>write and use user-defined functions
 * </ul>
 *
 * @see <a href="www.openbsd.org/cgi-bin/man.cgi?query=nc">netcat</a>
 */
public class SocketTextStreamWordCount {
    public static final String ARG_HOST = "localhost";
    public static final String ARG_PORT = "8125";
    //
    //	Program
    //

    public static void main(String[] args) throws Exception {


        // set up the execution environment
        final StreamExecutionEnvironment env = StreamExecutionEnvironment
                .getExecutionEnvironment();
        // get input data

        ParameterTool parameterTool = ParameterTool.fromArgs(args);

        env.setParallelism(Integer.parseInt(parameterTool.getRequired("parallelism")));

        DataStream<String> messageStream = env.addSource(new FlinkKafkaConsumer09<>(parameterTool.getRequired("topic"), new SimpleStringSchema(), parameterTool.getProperties()));
        DataStream<WordWithCount> windowCounts;

        switch (parameterTool.getRequired("part")) {
            case "aten":
                windowCounts = messageStream.map(new MapFunction<String, Tuple2<Integer, String>>() {
                    @Override
                    public Tuple2<Integer, String> map(String s) throws Exception {
                        return new Tuple2<>(s.length(), s);
                    }
                }).partitionCustom(new Aten(), 0).map(new MapFunction<Tuple2<Integer,String>, String>() {
                    @Override
                    public String map(Tuple2<Integer, String> stringTuple1) throws Exception {
                        return stringTuple1.getField(1);
                    }
                })
                        .flatMap(new DistributedThroughputLogger(1000) {
                            @Override
                            public void flatMap(String element, Collector<WordWithCount> collector) throws Exception {
                                for (String word : element.split("\\s")) {
                                    collector.collect(new WordWithCount(word, 1L));
                                }
                                super.flatMap(element, collector);
                            }
                        })
                        .keyBy("word")
                        .reduce(new ReduceFunction<WordWithCount>() {
                            @Override
                            public WordWithCount reduce(WordWithCount a, WordWithCount b) {
                                return new WordWithCount(a.word, a.count + b.count);
                            }
                        });
                break;
            case "smart":
                windowCounts = messageStream.map(new MapFunction<String, Tuple2<Integer, String>>() {
                    @Override
                    public Tuple2<Integer, String> map(String s) throws Exception {
                        return new Tuple2<>(s.length(), s);
                    }
                }).partitionCustom(new Smart(), 0).map(new MapFunction<Tuple2<Integer,String>, String>() {
                    @Override
                    public String map(Tuple2<Integer, String> stringTuple1) throws Exception {
                        return stringTuple1.getField(1);
                    }
                })
                        .flatMap(new DistributedThroughputLogger(1000) {
                            @Override
                            public void flatMap(String element, Collector<WordWithCount> collector) throws Exception {
                                for (String word : element.split("\\s")) {
                                    collector.collect(new WordWithCount(word, 1L));
                                }
                                super.flatMap(element, collector);
                            }
                        })
                        .keyBy("word")
                        .reduce(new ReduceFunction<WordWithCount>() {
                            @Override
                            public WordWithCount reduce(WordWithCount a, WordWithCount b) {
                                return new WordWithCount(a.word, a.count + b.count);
                            }
                        });
                break;
            case "random":
                windowCounts = messageStream.shuffle()
                        .flatMap(new DistributedThroughputLogger(1000) {
                            @Override
                            public void flatMap(String element, Collector<WordWithCount> collector) throws Exception {
                                for (String word : element.split("\\s")) {
                                    collector.collect(new WordWithCount(word, 1L));
                                }
                                super.flatMap(element, collector);
                            }
                        })
                        .keyBy("word")
                        .reduce(new ReduceFunction<WordWithCount>() {
                            @Override
                            public WordWithCount reduce(WordWithCount a, WordWithCount b) {
                                return new WordWithCount(a.word, a.count + b.count);
                            }
                        });
                break;
            case "rebalance":
                windowCounts = messageStream.rebalance()
                        .flatMap(new DistributedThroughputLogger(1000) {
                            @Override
                            public void flatMap(String element, Collector<WordWithCount> collector) throws Exception {
                                for (String word : element.split("\\s")) {
                                    collector.collect(new WordWithCount(word, 1L));
                                }
                                super.flatMap(element, collector);
                            }
                        })
                        .keyBy("word")
                        .reduce(new ReduceFunction<WordWithCount>() {
                            @Override
                            public WordWithCount reduce(WordWithCount a, WordWithCount b) {
                                return new WordWithCount(a.word, a.count + b.count);
                            }
                        });
                break;
            case "rescale":
                windowCounts = messageStream.rescale()
                        .flatMap(new DistributedThroughputLogger(1000) {
                            @Override
                            public void flatMap(String element, Collector<WordWithCount> collector) throws Exception {
                                for (String word : element.split("\\s")) {
                                    collector.collect(new WordWithCount(word, 1L));
                                }
                                super.flatMap(element, collector);
                            }
                        })
                        .keyBy("word")
                        .reduce(new ReduceFunction<WordWithCount>() {
                            @Override
                            public WordWithCount reduce(WordWithCount a, WordWithCount b) {
                                return new WordWithCount(a.word, a.count + b.count);
                            }
                        });
                break;
            case "broadcast":
                windowCounts = messageStream.broadcast()
                        .flatMap(new DistributedThroughputLogger(1000) {
                            @Override
                            public void flatMap(String element, Collector<WordWithCount> collector) throws Exception {
                                for (String word : element.split("\\s")) {
                                    collector.collect(new WordWithCount(word, 1L));
                                }
                                super.flatMap(element, collector);
                            }
                        })
                        .keyBy("word")
                        .reduce(new ReduceFunction<WordWithCount>() {
                            @Override
                            public WordWithCount reduce(WordWithCount a, WordWithCount b) {
                                return new WordWithCount(a.word, a.count + b.count);
                            }
                        });
                break;
            default:
                windowCounts = messageStream
                        .flatMap(new DistributedThroughputLogger(1000) {
                            @Override
                            public void flatMap(String element, Collector<WordWithCount> collector) throws Exception {
                                for (String word : element.split("\\s")) {
                                    collector.collect(new WordWithCount(word, 1L));
                                }
                                super.flatMap(element, collector);
                            }
                        })
                        .keyBy("word")
                        .reduce(new ReduceFunction<WordWithCount>() {
                            @Override
                            public WordWithCount reduce(WordWithCount a, WordWithCount b) {
                                return new WordWithCount(a.word, a.count + b.count);
                            }
                        });
        }

        windowCounts.flatMap(new ThroughputLogger(1000) {
            @Override
            public void flatMap(WordWithCount element, Collector<WordWithCount> collector) throws Exception {
                super.flatMap(element, collector);
            }
        }).setParallelism(1);

        //windowCounts.print();

        // execute program
        env.execute("Java WordCount from Kafka");

    }

    //
    // 	User Functions
    //
    public static class Smart implements Partitioner<Integer> {
        @Override
        public int partition(Integer size, int i) {
            return size % i;
        }
    }

    public static class Aten implements Partitioner<Integer> {
        private int[] returnArray = new int[] {-1};
        @Override
        public int partition(Integer key, int numPartitions) {
            if (numPartitions % 2 == 0)
                returnArray[0] += 1;
            else
                returnArray[0] += numPartitions;
            return (key * returnArray[0]) % numPartitions ;
        }
    }
    // ------------------------------------------------------------------------

    /**
     * Data type for words with count.
     */
    public static class WordWithCount {

        public String word;
        public long count;

        public WordWithCount() {
        }

        public WordWithCount(String word, long count) {
            this.word = word;
            this.count = count;
        }

        @Override
        public String toString() {
            return word + " : " + count;
        }
    }


}
