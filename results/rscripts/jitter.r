library(ggplot2)
library(plyr)
library(dplyr)
library(tidyr)
library(lubridate)
library(magrittr)
library(sfsmisc)

path = "/home/souza/Documents/burstflow/2017-10-03:18:05:00/"
#path = "C:/Users/Paulo/Google Drive/ScriptsR/newTests"
#path = "/home/souza/Documents/batchbased/Sex-Set01/"
#path = "C:/Users/Paulo/Documents/burstflow/2017-10-03_18_05_00"
setwd(path)

#label <- "Logfile_2017-08-31_3A17_3A03_3A57"
label <- "Logfile_2017-10-03:18:05:00"

df <- read.csv(label, header=T, sep="," )

df <- df %>%  group_by(latency, agg) %>%
  filter(!(abs(flinktime.ms. - median(flinktime.ms.)) > 2*sd(flinktime.ms.)))

aux <- df[df$type == "batch" ,]
aux <- aux[aux$latency != 10, ]
aux <- aux[aux$latency != 50, ]

p <- df %>%  group_by(latency, agg) %>%
  summarise_each(funs(mean), flinktime.ms., pythontime)

aux <- 1
for(i in 1:length(p$agg)){
  if(p$agg[i] == 0)
    aux <- i
  p$gain[i] <-  (((p$flinktime.ms.[aux]-p$flinktime.ms.[i])*100)/p$flinktime.ms.[aux])-100
}

df$eventDuration <- df$flinktime.ms./(13178407*8)

label <- ""

ggplot(df, aes(x = factor(latency), y = eventDuration , fill=factor(agg))) + 
  geom_bar(stat="identity", position = position_stack(reverse = F)) +
  theme_bw()+ theme(text = element_text(size=20)) +
  labs(x = "Latency", y = "Event duration (ms)", fill="Batch Size") + scale_fill_brewer(palette="OrRd")

ggsave(paste(label, "-Jitter.png", sep=""))

#ggplot(p,aes(x=parallelism %>% factor, y=flinktime.ms. , colour = agg, group = agg, shape = agg  %>% factor))+
#  geom_jitter()+ ggtitle(label) +  scale_shape_manual(values=seq(7,15))

#ggsave(paste(label, "Mean-Jitter.png", sep=""))

ggplot(p , aes(agg, flinktime.ms., group=latency%>%factor, colour=latency%>%factor)) + geom_jitter() +
  labs(x="Aggregation", y="Time (ms)")

ggplot(df, aes(factor(agg), flinktime.ms., group=factor(agg), colour=factor(latency), shape=factor(latency))) + geom_jitter(size=4) +
  geom_smooth(method = "auto", alpha=0, aes(group=factor(latency))) + theme_bw()+
  scale_colour_brewer(palette = "Set1") +
  scale_shape_manual(values=seq(0,9)) + 
  labs(x="Batch Size", y="Time (ms)", colour="Latency", shape="Latency")+
  theme(legend.position = c(0.75, 0.9), legend.direction = "horizontal", text = element_text(size=20))+
  guides(shape = guide_legend(override.aes = list(size = 3))) +
  guides(fill = guide_legend(override.aes = list(linetype = 0)),
         color = guide_legend(override.aes = list(linetype = 0)))

  ggplot(aux , aes(factor(agg), flinktime.ms., group=agg, colour=as.factor(latency))) + geom_jitter()  +
  geom_smooth(method = "auto", aes(group=1), alpha=0.1) +
  labs(x="Aggregation", y="Time (ms)") 

ggsave(paste(label, "-Boxplot.png", sep=""))

fm <- function(l){
  res <- rep(0, 2048)
  for(i in 1:2048){
    res[i] <- (l/(140*i))+i
  }
  print(min(res))
  print(which.min(res))
}

#speed up
p <- p[order(p$latency),] 

aux <- 1
for(i in 1:length(p$agg)){
  if(p$agg[i] == 0)
    aux <- i
  p$spu[i] <-  (1-(p$flinktime.ms.[i]/p$flinktime.ms.[aux]))*100
}

ggplot(p[p$agg!=0,], aes(x=agg, y=spu, color=latency)) +
  geom_point(position="dodge") +
  geom_line(aes(group=latency)) +
  facet_wrap(~latency)

speedup <- p[p$agg!=0,]
p[p$agg==0,]$parallelism <- p[p$agg==0,]$parallelism %>% factor

ggplot(speedup, aes(x=agg %>% factor, y=spu)) +
  geom_point(position="dodge") +
  geom_line(aes(group=agg)) +
  facet_wrap(~latency) + 
  labs(y="Speed up", x = "Parallelism") +
  annotate(geom='line', x=p[p$agg==0,]$parallelism, y=p[p$agg==0,]$spu, color="red", linetype = "dashed")

ggsave("SpeedUp.pdf")
x
#eficiency

aux <- 1
for(i in 1:length(p$parallelism)){
  p$efi[i] <-  p$spu[i]/p$parallelism[i]
}

ggplot(p, aes(x=as.factor(parallelism), y=efi, color=agg)) +
  geom_point(position="dodge") +
  geom_line(aes(group=agg)) +
  facet_wrap(~agg)
