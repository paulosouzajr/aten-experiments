require(ggplot2)
require(zoo)
require(dplyr)
path = "C:/Users/Paulo/Documents/dispatcher/results/logs"

#path = "/media/sf_results/logs"

#types <- c("1default2", "6random7", "5rescale6", "3rebalance4", "4broadcast5")


#path = "~/Documents/dispatcher/dispatcher/results/logs"

setwd(path)

types <- c("13default", "7rescale", "3rebalance", "8broadcast", "6random")
typesName <- c("Default", "Rescale", "Rebalance", "Broadcast", "Random")

names <- c("Master", "Slave1", "Slave2", "Slave3")
machine <- c("smart-f-master-", "smart-f-slave1-", "smart-f-slave2-", "smart-f-slave3-")
allData <- data.frame()

for(i in 1:length(types)){
  
  require(plyr)
  fNames <- c(paste(machine[1], types[i], ".log.csv", sep =""), 
              paste(machine[2], types[i], ".log.csv", sep =""), 
              paste(machine[3], types[i], ".log.csv", sep =""), 
              paste(machine[4], types[i], ".log.csv", sep =""))
  
  partData <- do.call(rbind, # Read the data and combine into single data frame
                      lapply(fNames,
                             function(f){
                               cbind(file=f, read.csv(f, header = TRUE, sep = " "))
                             }))
  #partData$ms <- do.call(c, tapply(partData$ms, partData$file, FUN=cumsum))
  partData$type <- types[i]
  #partData$sum <- cumsum(as.numeric(as.character(partData$ms)))
  partData$cont <- rep(1:length(partData$ms), each = 150, len = length(partData$ms))

  partData$file <- mapvalues(partData$file, from = fNames, to = names)
  partData$type <- mapvalues(partData$type, from = types, to = typesNames)
  detach(package:plyr)
  partData <- partData %>% group_by(file) %>% mutate(csum=cumsum(ms))
  partData <- partData %>% group_by(type, file, cont) %>% summarize(ms = sum(as.numeric(csum)), er = sum(er))
  
  require(plyr)
  partData
  allData <- rbind.fill(allData, partData)
}

detach(package:plyr)
aux <- allData %>% group_by(type, file) %>% summarize(max = max(ms), min = min(ms))


allData <- rbind(allData, data.frame(type = aux$type, file = aux$file, cont = 0, ms = aux$max + 1, er = 0))

allData <- rbind(allData, data.frame(type = aux$type, file = aux$file, cont = 0, ms = aux$min - 1, er = 0))

ggplot(allData, aes(x=ms/1000, y=el, colour=file))+
  geom_line() + geom_point(aes(shape=file), size = 1.5) + labs(y ="Elements", x="Execution Time (seconds)", 
                                                               colour = "Machine", shape = "Machine") + 
  facet_wrap(~type, nrow = 5) + theme_minimal() + theme(legend.position="bottom")

ggplot(allData)+
  geom_line(aes(x=as.numeric(sum), y=as.numeric(er), colour=file)) + ggtitle("Random") +
  scale_y_continuous(breaks = round(seq(min(as.numeric(allData$er), na.rm = TRUE), max(as.numeric(allData$er), na.rm = TRUE), by = 2000),1)) +
  facet_wrap(~type)
