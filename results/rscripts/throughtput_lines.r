library(ggplot2)
path = "C:/Users/Paulo/Documents/dispatcher/results"

#path = "C:/Users/Paulo/Documents/dispatcher"

#path = "~/Documents/dispatcher/dispatcher/results"

setwd(path)

broadcast <- read.csv("bc2", header=F, sep="" )
random <- read.csv("random2.csv", header=F, sep=" " )
rescale <- read.csv("rescale.csv", header=F, sep=" " )
roundrobin <- read.csv("rr.csv", header=F, sep=" " )

broadcast$type <- "broadcast"
random$type <- "random"
rescale$type <- "rescale"
roundrobin$type <- "roundrobin"

colnames(broadcast) <- c("ms", "value", "freq", "type")
colnames(random) <- c("ms", "value", "freq", "type")
colnames(rescale) <- c("ms", "value", "freq", "type")
colnames(roundrobin) <- c("ms", "value", "freq", "type")

broadcast$pos <- 1:length(broadcast$ms)
random$pos <- 1:length(random$ms)
rescale$pos <- 1:length(rescale$ms)
roundrobin$pos <- 1:length(roundrobin$ms)

broadcast$ms <- broadcast$ms/1000/60
random$ms <- random$ms/1000/60
rescale$ms <- rescale$ms/1000/60
roundrobin$ms <- roundrobin$ms/1000/60

broadcast <- timefix(broadcast)
random <- timefix(random)
rescale <- timefix(rescale)
roundrobin <- timefix(roundrobin)

df <- rbind(broadcast, random, rescale, roundrobin)

ggplot(data = df, aes(x=ms, y=as.numeric(value), colour=type)) + geom_point()

timefix <- function(data){
  first <- data$ms[1]
  data$ms[1] <- 0
  for(i in 2:length(data)){
    data$ms[i] <- data$ms[i] - first
  }
  return(data)
}
