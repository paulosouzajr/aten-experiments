require(ggplot2)
require(zoo)
require(dplyr)
require(stringr)
path = "~/Documents/dispatcher/results/rscripts"
setwd(path)

design <- read.csv("Secondo.csv", header = TRUE, sep = ",")

path = "~/Documents/testes/logs"
setwd(path)

machines <- c("smart-f-master-", "smart-f-slave1-", "smart-f-slave2-", "smart-f-slave3-")

maindf <- data.frame()

for(machine in machines){
  
  filenames <- paste(machine, c(1:140), as.character(design$type), ".log.csv", sep = "")
  #print(filenames)

  for(f in filenames){
    pos <- abs(as.numeric(str_extract(f, "\\-\\d+")))
    aux <- read.csv(f, header = TRUE, sep = " ")

    if(length(aux[[1]]) > 0){
      
      aux$cont <- 1#rep(1:length(aux[[1]]), each = 10000, len = length(aux[[1]]))
      
      #aux <- aux %>% group_by(cont) %>% mutate(csum=cumsum(as.numeric(ms)))

      aux <- aux %>% mutate(csum=cumsum(as.numeric(ms)))

      #aux <- aux %>% group_by(cont) %>% summarize(ms = sum(as.numeric(csum)), er = sum(as.numeric(er)))
      
      #aux$cont <- 1:length(aux[[1]])
      aux$type <- as.character(design$type[pos])
      aux$size <- design$size[pos]
      aux$machine <- machine
      
    
      maindf <- rbind(maindf, aux)
    }
  }
}

maindf$ms <- as.numeric(maindf$ms)
maindf$er <- as.numeric(maindf$er)

times <- allData[complete.cases(allData), ] %>% group_by(machine, type, size) %>% summarize(max = max(csum), min = min(csum))

times <- times %>% group_by(type, size) %>% summarize(max = max(max), min = min(min))

eventduration <- allData35 %>% group_by(type, size) %>% summarize(max = max(csum), sum = sum(ms), avg = max/sum)

allData <- maindf %>% group_by(machine, type, size, cont) %>% summarize_all(funs(mean))

aux <- allData %>% group_by(type, size) %>% summarize(max = max(ms), min = min(ms), mean = mean(er))

allData3 <- rbind(allData, allData2[allData2$machine == 'f-slave2-', ])

baseline <- allData[allData$type == 'default',]

burstflow <- allData[allData$type == 'smart', ]

parco <- rbind(baseline[baseline$size == 1,], burstflow[burstflow$size == 128,])

parco[parco$type == 'naive',]$type <- 'burstflow'
parco[parco$type == 'default',]$type <- 'baseline'



ggplot(allData, aes(x=csum/1000, y=er, fill=machine))+
  geom_area() + labs(y ="Elements", x="Execution Time (seconds)",
                     colour = "Machine", shape = "Machine") +
  facet_wrap(type~size) + theme(legend.position="bottom", axis.text=element_text(size=15),
                                legend.direction = "horizontal",
                                axis.title=element_text(size=15),
                                text = element_text(size = 15),
                                legend.text=element_text(size=15),
                                plot.title=element_text(size=15))  + scale_fill_grey()


ggplot(allData, aes(x=cont, y=er, fill=machine))+
  geom_area() + labs(y ="Elements", x="Execution Time (seconds)",
                                                          colour = "Machine", shape = "Machine") +
  facet_wrap(type~size) + theme(legend.position="bottom", axis.text=element_text(size=15),
                                                  legend.direction = "horizontal",
                                                  axis.title=element_text(size=15),
                                                  text = element_text(size = 15),
                                                  legend.text=element_text(size=15),
                                                  plot.title=element_text(size=15))  + scale_fill_grey()

allData <- allData[(allData$type != 'aten'), ]
allData[allData$type == 'smart',]$type <- 'aten'
allData[allData$type == 'aten',]$type <- 'naive'
allData[allData$type == 'default',]$type <- 'baseline'
allData <- allData[-c(106899:106904),]

allData2$machine <- gsub("smart-f", "f", allData2$machine)
allData2$size <- gsub("128", "No Buffering", allData2$size)
allData2$size <- gsub("1", "Using Buffering", allData2$size)

ggplot(aux, aes(type, mean) )+
  geom_bar(position = "dodge") + labs(y="Network received (Mb)", x="Time (s)")


pdata2 <- allData125 %>% 
  group_by(machine, type) %>% 
  summarise(new = list(mean_se(er, mult=0.95)), mean = mean(er), sd=sd(er)) %>% 
  unnest(new)


pdata11 <- pdata3

pdata3[pdata3$type == 'baseline',]$type <- 'aten'
pdata3[pdata3$type == 'naive',]$type <- 'baseline'
pdata3[pdata3$type == 'aten',]$type <- 'naive'


# data[data$type == 'naive',]$y <- data[data$type == 'naive',]$y - 200  
# data[data$type == 'naive',]$ymin <- data[data$type == 'naive',]$ymin - 200
# data[data$type == 'naive',]$ymax <- data[data$type == 'naive',]$ymax - 200

pdata2 %>% 
  ggplot(aes(type, y = y, fill = machine)) +
  geom_col(position = "dodge") +
  geom_errorbar(aes(ymin = mean-sd, ymax = mean+sd), position = "dodge") + 
  scale_fill_brewer(palette="Blues") + labs(x = "Partition Algorithm", y = "Events Average") + theme(legend.position="bottom", axis.text=element_text(size=15),
                                                                                                                       legend.direction = "horizontal",
                                                                                                                       axis.title=element_text(size=15),
                                                                                                                       text = element_text(size = 15),
                                                                                                                       legend.text=element_text(size=13),
                                                                                                                       plot.title=element_text(size=15)) + scale_fill_grey()

baseline1 <- allData35[allData35$size == 'No Buffering',]

baseline1 <- baseline1[baseline1$type == 'baseline',]

allData125 <- allData35[allData35$size == 'Using Buffering', ]

allData125 <- allData125[allData125$type != 'baseline',]

allData125 <- rbind(baseline1, allData125)

pdata2 <- parco %>% 
  group_by(machine, type) %>% 
  summarise(new = list(mean_se(er)), mean = mean(er), sd=sd(er)) %>% 
  unnest(new)

new %>% 
  ggplot(aes(type, y =ms/10)) +
  geom_boxplot(position = "dodge") + scale_fill_brewer(palette="Blues") + labs(x = "Partition Algorithm", y = "Single event duration (ms)") + scale_y_log10()

new[new$type != 'baseline', ]$ms <- new[new$type != 'baseline', ]$ms/128

which.max(new$ms)
max(new$ms)
new[102923:102950,]
#new <- saveme

new<- new[-c(102923:102924), ]

pdata2 %>% 
  ggplot(aes(machine, y = y, fill=type)) +
  geom_col(position = "dodge") +
  scale_fill_brewer(palette="Blues") + theme_minimal() + labs(x = "Partition Algorithm", y = "Events Average") + theme(legend.position="bottom", axis.text=element_text(size=15),
                                                                                                                       legend.direction = "horizontal",
                                                                                                                       axis.title=element_text(size=15),
                                                                                                                       text = element_text(size = 15),
                                                                                                                       legend.text=element_text(size=13),
                                                                                                                       plot.title=element_text(size=15)) + scale_fill_grey()
eventduration[eventduration$type == "naive", ]$avg <- eventduration[eventduration$type == "naive", ]$avg * 128 

ggplot(eventduration,aes(x=type,y=avg*100))+
  geom_bar(stat="identity",position="dodge")+ scale_fill_brewer(palette="Blues")+
    labs(x = "Algorithm", y = "Single event duration (ms)", fill="")  + scale_fill_grey() + theme(legend.position="bottom")



aux2 <- allData125 %>% group_by(type) %>% summarize(max = max(ms), min = min(ms), soma = sum(er), mean=max/soma)

which.max(allData3$ms)
