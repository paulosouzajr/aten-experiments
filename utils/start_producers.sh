#!/bin/bash

echo "Starting producers"

ssh -f azureuser@smart-p2 "sudo sh -c 'nohup python3 kafkaproducer.py $1 $2 $3 $4 &'"
ssh -f azureuser@smart-p3 "sudo sh -c 'nohup python3 kafkaproducer.py $1 $2 $3 $4 &'"
ssh -f azureuser@smart-p4 "sudo sh -c 'nohup python3 kafkaproducer.py $1 $2 $3 $4 &'"

python3 kafkaproducer.py $1 $2 $3 $4

echo "We're done here"
