#!/bin/bash
#Parse, Save and Clean logs from slaves nodes

echo "Copying logs"

cp /home/azureuser/flink-1.4.0/log/flink-azureuser-taskmanager-0-SMART-F-Master.log logs/smart-f-master-$1.log
scp smart-f-slave1:/home/azureuser/flink-1.4.0/log/*.log logs/smart-f-slave1-$1.log
scp smart-f-slave2:/home/azureuser/flink-1.4.0/log/*.log logs/smart-f-slave2-$1.log
scp smart-f-slave3:/home/azureuser/flink-1.4.0/log/*.log logs/smart-f-slave3-$1.log

echo "Clear flink log files"

echo ' ' > /home/azureuser/flink-1.4.0/log/flink-azureuser-taskmanager-0-SMART-F-Master.log
ssh smart-f-slave1 "echo '' > /home/azureuser/flink-1.4.0/log/*.log"
ssh smart-f-slave2 "echo '' > /home/azureuser/flink-1.4.0/log/*.log"
ssh smart-f-slave3 "echo '' > /home/azureuser/flink-1.4.0/log/*.log"

echo "Wipe Buffer and Cache"

sudo sh -c 'echo 1 >/proc/sys/vm/drop_caches'
sudo sh -c 'echo 2 >/proc/sys/vm/drop_caches'
sudo sh -c 'echo 3 >/proc/sys/vm/drop_caches'

ssh smart-f-slave1 "sudo sh -c 'echo 1 >/proc/sys/vm/drop_caches'; sudo sh -c 'echo 2 >/proc/sys/vm/drop_caches'; sudo sh -c 'echo 3 >/proc/sys/vm/drop_caches'"
ssh smart-f-slave2 "sudo sh -c 'echo 1 >/proc/sys/vm/drop_caches'; sudo sh -c 'echo 2 >/proc/sys/vm/drop_caches'; sudo sh -c 'echo 3 >/proc/sys/vm/drop_caches'"
ssh smart-f-slave3 "sudo sh -c 'echo 1 >/proc/sys/vm/drop_caches'; sudo sh -c 'echo 2 >/proc/sys/vm/drop_caches'; sudo sh -c 'echo 3 >/proc/sys/vm/drop_caches'"

echo "Parsing collected logs"

for f in $(ls logs/smart*.log); do
        echo "Parsing.. $f"
	echo "ms er mb" >> $f.csv
        cat $f |grep -a "During"|cut -d " " -f 49,53,58 >> $f.csv
        mv $f logs/raw_data
done
