import socket
import time
import os
import sys
from threading import *

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = "0.0.0.0"
port = int(sys.argv[1])
aggSize = int(sys.argv[2])
type = sys.argv[3]
serversocket.bind((host, port))

class client(Thread):
    def __init__(self, socket, address, aggSize, barrier, type):
        Thread.__init__(self)
        self.sock = socket
        self.addr = address
        self.aggSize = aggSize
        self.barrier = barrier
        self.type = type
        self.start()

    def run(self):
        print("Running with {}".format(self.aggSize) + " and type {}".format(self.type))
        start = time.time()
        agg = 0
        offset = 0
        if(self.aggSize == 0): self.aggSize = 1
        blocksize = self.aggSize * 1048576
        concat = bytearray()
        pathdir = '/home/ubuntu/python/twitter'
        #pathdir = 'C:\\Users\\Paulo\\Documents\\dataset'
        for filename in os.listdir(pathdir):
            with open(pathdir + '/' +filename, 'rb') as fp:
                if(self.type == "stream"):
                    #print("stream type")
                    for line in fp:
                        concat += line
                        agg += 1
                        if(agg > self.aggSize):
                            self.sock.sendall(concat)
                            agg = 0
                            concat = bytearray()
                else:
                    #print("Batch type")
                    if(self.aggSize == 1): blocksize = 4096 * 1048576
                    while True:
                        sent = self.sock.sendfile(fp, offset, blocksize)
                        #print(sent)
                        if sent == 0:
                            break  # EOF
                        offset += sent
        if(agg > 0):
            self.sock.send(concat)
            agg=0;
        end = time.time()
        print(current_thread().name,
          'waiting for barrier with {} others'.format(
              self.barrier.n_waiting))
        self.barrier.wait()
        self.sock.close()
        print ("Time: {} ".format(end - start) + "Agg: {} ".format(self.aggSize))

serversocket.listen(1)
NUM_THREADS = 2
barrier = Barrier(NUM_THREADS)
clientsocket, address = serversocket.accept()
for i in range(0, NUM_THREADS):
    client(clientsocket, address, aggSize, barrier, type)
