from kafka import KafkaProducer
import os
import sys
import time
from threading import *

host = sys.argv[1]
port = int(sys.argv[2]) # default = 9092
queue = sys.argv[3]
aggSize = int(sys.argv[4])
threads = int(sys.argv[5])

if len(sys.argv) != 6:
	print("Usage HOST PORT QUEUE_NAME AGGSIZE NUM_THREADS ")	
	sys.exit(0)

class client(Thread):
    def __init__(self, producer, queue, aggSize, barrier, type):
        Thread.__init__(self)
        self.topic = queue
        self.aggSize = aggSize
        self.barrier = barrier
        self.type = type
        self.producer = producer
        self.start()

    def run(self):
        print("Running with {}".format(self.aggSize) + " and type {}".format(self.type))
        start = time.time()
        pathdir = '/home/souza/Documents/ERAD2017/dataset'
        #pathdir = 'C:\\Users\\Paulo\\Documents\\dataset'
        for filename in os.listdir(pathdir):
            with open(pathdir + '/' +filename, 'rb') as fp:
            	for line in fp:
            		#print(line)
            		producer.send(self.topic, b'%s' % line)
        end = time.time()
        print(current_thread().name,
          'waiting for barrier with {} others'.format(
              self.barrier.n_waiting))
        self.barrier.wait()
        producer.close()
        print ("Time: {} ".format(end - start) + "Agg: {} ".format(self.aggSize))

# To send messages asynchronously
#client = SimpleClient("{}:{}".format(host, port))
# To send messages in batch. You can use any of the available
# producers for doing this. The following producer will collect
# messages in batch and send them to Kafka after 20 messages are
# collected or every 60 seconds
# Notes:
# * If the producer dies before the messages are sent, there will be losses
# * Call producer.stop() to send the messages and cleanup
#producer = SimpleProducer(client,
#                         async=True,
#                         batch_send_every_n=aggSize)

producer = KafkaProducer(bootstrap_servers=['{}:{}'.format(host, port)], batch_size=aggSize*1024)

NUM_THREADS = threads
barrier = Barrier(NUM_THREADS)
for i in range(0, NUM_THREADS):
    client(producer, queue, aggSize, barrier, type)
